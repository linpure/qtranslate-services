# QTranslateServices

#### 介绍
QTranslate翻译软件的Services。由于官方更新不是很及时且并未开源，想自己修复失效接口只有修改Service.js这唯一的办法了。通过此仓库大家可以分享自己可用的Services

#### 软件架构
软件架构说明


#### 安装教程

1.  下载仓库中对应Service
2.  替换软件安装目录下Services中对应文件
3.  重启QTranslate

#### 使用说明

1.  接口可能失效，不保证长期可用
2.  不同版本可能有差异，不保证所有版本通用，尽量使用新版本软件
3.  注意备份原文件

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
