function serviceHeader() {
  return new ServiceHeader(
    28,
    "Baidu",
    "Baidu Translate is a free online translation service." +
      Const.NL2 +
      serviceHost() +
      Const.NL2 +
      "\u00a9 2018 Baidu",
    Capability.TRANSLATE | Capability.DETECT_LANGUAGE | Capability.LISTEN
  );
}
function serviceHost(b, d, e) {
  return b === Capability.LISTEN
    ? "https://tts.baidu.com"
    : "https://fanyi.baidu.com";
}
function serviceLink(b, d, e) {
  var g = serviceHost();
  b &&
    ((d = isLanguage(d) ? codeFromLanguage(d) : "auto"),
    (e = isLanguage(e) ? codeFromLanguage(e) : "auto"),
    (g += format("/#{0}/{1}/{2}", d, e, encodeGetParam(b))));
  return g;
}
SupportedLanguages = [
  -1,
  -1,
  -1,
  -1,
  -1,
  "ara",
  -1,
  -1,
  -1,
  "bul",
  -1,
  "zh",
  "cht",
  -1,
  "cs",
  "dan",
  "nl",
  "en",
  "est",
  "fin",
  -1,
  "fra",
  -1,
  "de",
  "el",
  -1,
  -1,
  -1,
  "hu",
  -1,
  -1,
  "it",
  -1,
  "jp",
  -1,
  "kor",
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  "pl",
  "pt",
  "rom",
  "ru",
  -1,
  -1,
  "slo",
  "spa",
  -1,
  "swe",
  "th",
  -1,
  -1,
  -1,
  "vie",
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
];
function a(b, d) {
  for (var e = 0; e < d.length - 2; e += 3) {
    var g = d.charAt(e + 2),
      g = "a" <= g ? g.charCodeAt(0) - 87 : Number(g),
      g = "+" === d.charAt(e + 1) ? b >>> g : b << g;
    b = "+" === d.charAt(e) ? (b + g) & 4294967295 : b ^ g;
  }
  return b;
}
function sign(b, d) {
  var e = b.length;
  30 < e &&
    (b =
      "" +
      b.substr(0, 10) +
      b.substr(Math.floor(e / 2) - 5, 10) +
      b.substr(b.length - 10));
  var g = d.split("."),
    e = Number(g[0]) || 0,
    g = Number(g[1]) || 0,
    f,
    c = e;
  for (f = 0; f < b.length; f++) {
    var h = b.charCodeAt(f);
    128 > h
      ? (c = a(c + h, "+-a^+6"))
      : (2048 > h
          ? (c = a(c + ((h >> 6) | 192), "+-a^+6"))
          : (55296 == (64512 & h) &&
            f + 1 < b.length &&
            56320 == (64512 & b.charCodeAt(f + 1))
              ? ((h = 65536 + ((1023 & h) << 10) + (1023 & b.charCodeAt(++f))),
                (c = a(c + ((h >> 18) | 240), "+-a^+6")),
                (c = a(c + (((h >> 12) & 63) | 128), "+-a^+6")))
              : (c = a(c + ((h >> 12) | 224), "+-a^+6")),
            (c = a(c + (((h >> 6) & 63) | 128), "+-a^+6"))),
        (c = a(c + ((63 & h) | 128), "+-a^+6")));
  }
  c = a(c, "+-3^+b+-f");
  c ^= g;
  0 > c && (c = (2147483647 & c) + 2147483648);
  c %= 1e6;
  return c.toString() + "." + (c ^ e);
}
function serviceDetectLanguageRequest(b) {
  b = limitSource(b);
  b = "query=" + encodeGetParam((b || "").slice(0, 100));
  var d = postHeader() + Const.NL + "Cookie: " + Options.BaiduCookie;
  return new RequestData(HttpMethod.POST, "/langdetect", b, d);
}
function serviceDetectLanguageResponse(b) {
  return (b = parseJSON(b)) && b.lan
    ? languageFromCode(b.lan)
    : UNKNOWN_LANGUAGE;
}
function serviceTranslateRequest(b, d, e) {
  b = prepareSource(b);
  b = limitSource(b);
  b = format(
    "query={0}&from={1}&to={2}&simple_means_flag=3&sign={3}&token={4}",
    encodeURIComponent(b).replace(/%20/g, "+"),
    codeFromLanguage(d),
    codeFromLanguage(e),
    // sign(b, Options.BaiduGtk),
    sign(b,"320305.131321201"),
    Options.BaiduToken
    // "a22087657ae6f8e983fa54e33f4ab040"
  );
  d = postHeader() + Const.NL + "Cookie: " + Options.BaiduCookie;
//   d = postHeader() + Const.NL + "Cookie: BIDUPSID=1E0FB2835EC9639A0E28A3A3BDB7B0EA; PSTM=1616573402; __yjs_duid=1_547205d5239e5d141d86ced82f5d9cb71619768759595; BDUSS=FMMTlLek1tQnZwR3E2Tk9hc0pndlFxcXEtUUlzNS12UEl1aWR5U0tRfkNVTkJnRVFBQUFBJCQAAAAAAAAAAAEAAADfPNedxsa3ydeoyvQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMLDqGDCw6hgTX; BDUSS_BFESS=FMMTlLek1tQnZwR3E2Tk9hc0pndlFxcXEtUUlzNS12UEl1aWR5U0tRfkNVTkJnRVFBQUFBJCQAAAAAAAAAAAEAAADfPNedxsa3ydeoyvQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMLDqGDCw6hgTX; REALTIME_TRANS_SWITCH=1; FANYI_WORD_SWITCH=1; HISTORY_SWITCH=1; SOUND_SPD_SWITCH=1; SOUND_PREFER_SWITCH=1; APPGUIDE_10_0_2=1; BAIDUID=7C0AAE5C3C41EAE61C0DF0A27A8EBA26:SL=0:NR=10:FG=1; H_PS_PSSID=; Hm_lvt_64ecd82404c51e03dc91cb9e8c025574=1662988020; BDORZ=FFFB88E999055A3F8A630C64834BD6D0; MCITY=-48%3A; BA_HECTOR=21al20a50hag218h2ka5ctpg1hjcsta1b; ZFY=qMGj8G1omZ1STML5sOjP6la8oxJNp8c4WheGRum:BRp0:C; BAIDUID_BFESS=7C0AAE5C3C41EAE61C0DF0A27A8EBA26:SL=0:NR=10:FG=1; Hm_lpvt_64ecd82404c51e03dc91cb9e8c025574=1664513214; ab_sr=1.0.1_ZmRjZTZjMzRjYmRkMDVlNTc2ZGQwMmU0N2Q0YWFkYmVhNjIxN2Y4ZTA5OWQxOTNiMmZhZGE2NzE4YTY0OGExOTZhZTk4NjIyY2QwMTI4NjFiMmViYjdjNGNkNTNkMTk4NDkzNjExOTI5ZjJmNmY0NWUwM2Y0ZWZhOGE1MDZiYjJhZWQ4Y2VmZmNkZTllNGQ3ZTEzODVlYjNlNmMyNDgxY2NlMTNhNmU3Y2Y2NjlhMmQzY2I5NmE1YWE4YjRiMzU1";

  return new RequestData(HttpMethod.POST, "/v2transapi", b, d);
}
function serviceTranslateResponse(b, d, e, g) {
  var f = parseJSON(d);
  b = "";
  var c;
  if (f) {
    if (f.trans_result) {
      e = f.trans_result.data;
      for (d = 0; d < e.length; d++) {
        for (c = 0; c < e[d].prefixWrap; c++) b += Const.NL;
        b += e[d].dst + Const.NL;
      }
      e = languageFromCode(f.trans_result.from);
      g = languageFromCode(f.trans_result.to);
    }
    if (f.dict_result && f.dict_result.simple_means) {
      var h = f.dict_result.simple_means;
      b += Const.NL;
      if (h && h.symbols)
        for (d = 0; d < h.symbols.length; d++)
          if ((f = h.symbols[d]) && f.parts)
            for (c = 0; c < f.parts.length; c++) {
                if(e == 11){
                    var p = f.parts[c].means;
                    if(p && p.length){
                       for (var index = 0; index < p.length; index++) {
                            var l = p[index];
                            l.text && (b += (index+1) + ". " + l.text);
                            if(l.means){
                                b += " (";
                                for (var i = 0; i < l.means.length; i++) {
                                    b += l.means[i];
                                    if(i < l.means.length-1){
                                        b += ",";
                                    } 
                                }
                                b += ")";
                            }
                            l.part && (b += " " + l.part);
                            b += Const.NL;
                        }
                        b += Const.NL;
                    }
                }else{
                    var k = f.parts[c];
                    k.part && (b += "[" + k.part + "] ");
                    b += k.means
                        .map(function (b) {
                        return b;
                        })
                        .join("; ");
                    b += Const.NL;
                }
              
            }
    } else if (f.dict_result && f.dict_result.content && f.dict_result.content)
      for (f = f.dict_result.content, b += Const.NL, d = 0; d < f.length; d++)
        if ((k = f[d]) && k.mean)
          for (c = 0; c < k.mean.length; c++) {
            var l = k.mean[c];
            b += l.pre + " ";
            for (h in l.cont) l.cont.hasOwnProperty(h) && (b += h);
            b += Const.NL;
          }
  }
  return new ResponseData(b, e, g);
}
function serviceListenRequest(b, d, e) {
  b = format(
    "/text2audio?lan={0}&ie=UTF-8&text={1}",
    codeFromLanguage(d),
    encodeGetParam(b)
  );
  e && (b += "&spd=1");
  return new RequestData(HttpMethod.GET, b);
}
